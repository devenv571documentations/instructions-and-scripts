# download anaconda-xy.sh
https://www.anaconda.com/distribution/

# PATH will be updated in ~/.bashrc, if other shell used, move to the apropriate file

# create environment
conda create --name my-env

# update conda
conda update -n base -c defaults conda

# disable default activation of base env
conda config --set auto_activate_base false

# active environment
conda activate my-env

# deactivate environment
conda deactivate

# list all conda environments
conda info --envs

# remove conda environment
conda env remove --name my-env

# Usage with IntelliJ

* conda environment can be created in intellij under 'project structure'
* under 'Project Settings' -> ' Project, select the correct SDK (from created environment)
* active environment in the terminal
* install packages (e.g. **conda install** numpy in terminal, make sure you are in the correct environment)
