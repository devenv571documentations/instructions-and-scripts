busybox httpd --help # show available options
busybox httpd -p 127.0.0.1:8080 -h /var/www/  # start httpd 
pkill busybox  # to stop busybox httpd
