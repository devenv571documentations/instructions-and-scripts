# Setup cluster with kubeadm and vagrant

## Controlplane

* initialize cluster (we assume Flannel as our networking solution, otherwise change the cidr accordingly)

<pre><code>
sudo kubeadm init --pod-network-cidr="10.244.0.0/16" --apiserver-advertise-address="192.168.178.200"

# --apiserver-advertise-address should be set to the ip of the host, the control plane is deployed on
# optional: --apiserver-cert-extra-sans="controlplane"
</code></pre>

* optional: use config file instead of options (can be helpful for setting up kubelet correctly with TLS)
<pre><code>
apiVersion: kubeadm.k8s.io/v1beta3
kind: InitConfiguration
localAPIEndpoint:
  advertiseAddress: 10.29.247.9
---
apiVersion: kubelet.config.k8s.io/v1beta1
kind: KubeletConfiguration
serverTLSBootstrap: true
---
apiVersion: kubeadm.k8s.io/v1beta3
kind: ClusterConfiguration
apiServer:
  certSANs:
  - controlplane
networking:
  podSubnet: "10.244.0.0/16"
</code></pre>

* move kubeconfig to home directory

<pre><code>
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
</code></pre>

* optional: patch the node to enable pod cidr (only necessary, if the option in the kubeadm init did not work for some reason)

<pre><code>
kubectl patch node k8s-master-1 -p '{"spec":{"podCIDR":"10.244.0.0/16"}}'
</code></pre>

* add Flannel as Networking Solution

<pre><code>
kubectl apply -f https://raw.githubusercontent.com/flannel-io/flannel/master/Documentation/kube-flannel.yml
</code></pre>

* optional: add Flannel config file (if Flannel complains in the logs)

<pre><code>
# /run/flannel/subnet.env

FLANNEL_NETWORK=10.244.0.0/16
FLANNEL_SUBNET=10.244.0.1/24
FLANNEL_MTU=1450
FLANNEL_IPMASQ=true
</code></pre>

## Join Worker Node

* run the following command on a worker not which should be added to the cluster

<pre><code>
kubeadm join 192.168.178.200:6443 --token <token> \
	--discovery-token-ca-cert-hash <discovery-token-ca-cert-hash>
</code></pre>

* to get the token, run the following command on the control plane

<pre><code>
kubeadm token list
</code></pre>

* if the token is expired, create a new one like this on the control plane

<pre><code>
kubeadm token create --print-join-command
</code></pre>

* get the discovery token on the control plane (remember you have to prefix it with sha256: in the join command)

<pre><code>
openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt | openssl rsa -pubin -outform der 2>/dev/null | \
   openssl dgst -sha256 -hex | sed 's/^.* //'
</code></pre>

* update config of the kubelet by adding the following line (e.g. needed to retrieve logs)

<pre><code>
# /etc/systemd/system/kubelet.service.d/10-kubeadm.conf

Environment="KUBELET_EXTRA_ARGS=--node-ip=<worker IP address>"
</code></pre>

* restart kubelet

<pre><code>
sudo systemctl daemon-reload
sudo systemctl restart kubelet
</code></pre>



## Troubleshooting

### DNS Resolution not Working

* restart coredns on control plane(to make sure dns resolution is working)

<pre><code>
kubectl -n kube-system rollout restart deployment coredns
</code></pre>

### Secure Communication from third party tools like Metrics Server not working

* per default, the kubelet certificates are self-signed (which makes communication with external service like Metrics Server insecure)
* https://kubernetes.io/docs/tasks/administer-cluster/kubeadm/kubeadm-certs/#kubelet-serving-certs

## Optional Components

### install Helm

<code>
curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
</code>

### install Ingress Nginx Controller

<code>
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.5.1/deploy/static/provider/cloud/deploy.yaml
</code>

* make sure the ips of the nodes are set as external ips in the spec part of the ingress-controller service (loadbalancer)

<code>
spec:
  externalIPs:
  - 10.0.0.11
  - 10.0.0.12
</code>


### install k9s on control plane

<pre><code>
export K9S_VERSION=0.26.7
wget https://github.com/derailed/k9s/releases/download/v$K9S_VERSION/k9s_Linux_x86_64.tar.gz
mkdir /tmp/k9s
tar -xzvf k9s_Linux_x86_64.tar.gz -C /tmp/k9s/
sudo mv /tmp/k9s/k9s /usr/local/bin/k9s
sudo rm k9s_Linux_x86_64.tar.gz
sudo rm -r /tmp/k9s/
</code></pre>

### install Metrics Server

<pre><code>
kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml

# if nothing works, add the following to the deployment
## add --kubelet-insecure-tls to the args
## and hostNetwork: true to the container spec
</code></pre>

### install Prometheus Stack

<pre><code>
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm install kube-prometheus-stack prometheus-community/kube-prometheus-stack -n monitoring
</code></pre>
