# On Docker Executor

## install components

* install Docker (see docker.sh)
* install gitlab runner

<pre>
<code>
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash

sudo apt-get install gitlab-runner
<code>
</pre>


## register the runner

<pre>
<code>
sudo gitlab-runner register # sudo is important in vagrant, as otherwise, the runner cannot be contacted
</code>
</pre>

* enter data from the Settings -> CI/CD page on gitlab
* **important:** choose "docker" as executor
* as default image, alpine:latest is a reasonable choice

* if runner is not green in the gitlab UI, try
<pre>
<code>
gitlab-runner verify
</code>
</pre>

### Custom CA Certificates

* to add CA certificates, update the /etc/gitlab-runner/config.toml file (in part [runners.docker])
* https://docs.gitlab.com/runner/configuration/tls-self-signed.html#trusting-tls-certificates-for-docker-and-kubernetes-executors

<pre>
<code>
volumes = ["/cache", "/path/to-ca-cert-dir/ca.crt:/etc/gitlab-runner/certs/ca.crt:ro"]
</code>
</pre>

### Custom CA Certificates with kaniko

* https://docs.gitlab.com/ee/ci/docker/using_kaniko.html#using-a-registry-with-a-custom-certificate

## monitor runner

<pre>
<code>
sudo journalctl -u gitlab-runner -f
</code>
</pre>


# Use kaniko to push Docker images to Docker Hub

* CI_REGISTRY should be set to 'https://index.docker.io/v2/'
** if v2 is not working, try v1

<pre>
<code>
build:
  stage: container_build
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [ "" ]
  script:
    - JAR_NAME=./target/app.jar
    - APP_NAME="ci-test-app"
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"${CI_REGISTRY}\":{\"auth\":\"$(printf "%s:%s" "${CI_REGISTRY_USER}" "${CI_REGISTRY_PASSWORD}" | base64 -w 0)\"}}}" > /kaniko/.docker/config.json
    - >-
      /kaniko/executor
      --context "${CI_PROJECT_DIR}"
      --dockerfile "${CI_PROJECT_DIR}/Dockerfile"
      --destination "devenv571/${APP_NAME}:latest"
  only:
    - master
</code>
</pre>

# Use Custom Docker Registry with Self-Signed Certificate

* make sure the certificate is on all nodes where the Gitlab Runner might be executed
* note: restart of the nodes might be necessary

<code>
sudo cp rootCA.crt /usr/local/share/ca-certificates/
sudo update-ca-certificates --fresh
</code>

