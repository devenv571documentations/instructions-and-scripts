#!/bin/bash

# update k9s to given version
k9s-update () {
  export K9S_VERSION=$1
  wget -O k9s.tar.gz https://github.com/derailed/k9s/releases/download/v$K9S_VERSION/k9s_Linux_amd64.tar.gz
  mkdir /tmp/k9s
  tar -xzvf k9s.tar.gz -C /tmp/k9s/
  mv /tmp/k9s/k9s $HOME/.local/bin
  rm k9s.tar.gz
}

