#!/bin/bash

# fast-forward git contributions if further refinement is not important
git-ff () {
  git add .
  git commit -m "$1"
  git pull --rebase --no-edit
  git push
}

