# Add local user folder to PATH
export PATH=/usr/local/bin:$PATH

# Adding nodejs to PATH
export PATH=$HOME/tools/nodejs/bin:$PATH

# Adding protoc to PATH
export PATH=$HOME/tools/protoc3/bin:$PATH

# Adding Maven to PATH
export PATH=$HOME/tools/apache-maven-3.8.6/bin:$PATH

# Load additional tools
export PATH=$HOME/tools/misc:$PATH

# load custom scripts
for FILE in $HOME/tools/scripts/*.sh; do source $FILE; done
