# install gcloud via package manager on debian/ubuntu

<pre>
<code>
echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list

curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -

sudo apt-get update && sudo apt-get install google-cloud-sdk
<code>
</pre>

# initialize gcloud

<pre>
<code>
gcloud init
</code>
</pre>

# manage projects

<pre>
<code>
gcloud projects list // list all projects
gcloud config set project my-project // change project
</code>
</pre>

# configure docker to be able to push to google cloud registry

<pre>
<code>
gcloud auth configure-docker

docker build -t gcr.io/[gcp-project]/[app-name]:[tag] .

docker push gcr.io/[gcp-project]/[app-name]:[tag]
</code>
</pre>


