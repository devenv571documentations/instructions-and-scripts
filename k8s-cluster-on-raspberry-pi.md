# Raspberry Pi setup

## change hostname and locale

* open **sudo raspi-config**
** change hostname (e.g. k8s-master-1, k8s-node-1, k8s-node-2 etc.)
** change locale
* reboot


## set static ip

<pre><code>
cat >> /etc/dhcpcd.conf
</code></pre>

* paste the following lines (adapt to router ip)

<pre><code>
interface eth0
static ip_address=x.x.x.y/24
static routers=x.x.x.1
static domain_name_servers=8.8.8.8
</code></pre>

* reboot

## disable swap (required by kubernetes)

<pre><code>
sudo dphys-swapfile swapoff && \
sudo dphys-swapfile uninstall && \
sudo update-rc.d dphys-swapfile remove
</code></pre>

* **sudo swapon --summary** should output nothing

# update boot parameters

* add the following to the file **/boot/cmdline.txt** (do not add any new lines)

<pre><code>
cgroup_enable=cpuset cgroup_memory=1 cgroup_enable=memory
</code></pre>

* reboot

## install k3sup (on k8s-management machine)

<pre><code>
curl -SLsf https://get.k3sup.dev | sudo sh
sudo chmod +x /usr/local/bin/k3sup
</code></pre>

### generate new ssh key (on k8s-management machine)

<pre><code>
ssh-keygen -t rsa -b 4096 -C "blubb@example.com"
</code></pre>

### move ssh key onto the raspberry pi(s)

<pre><code>
ssh-copy-id pi@$PI_IP
</code></pre>

### install cluster (server-ip is the ip of the master node)

<pre><code>
k3sup install --ip $SERVER_IP --user pi
</code></pre>

### add worker node to cluster (other raspberry)

<pre><code>
k3sup join --ip $AGENT_IP --server-ip $SERVER_IP --user pi
</code></pre>

### add worker node to cluster (e.g. vagrant box)

* Caution: to use a vagrant box as node, the used linux distro has to have an "up-to-date" kernel version (> 4.4 maybe?)
<pre><code>
# Vagrantfile
Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/focal64"
  config.vm.hostname = "k8s-node-3"
  # config.vm.network "public_network", ip: "192.168.178.103"
  config.disksize.size = '20GB'
  
  config.vm.define "ubuntu_k8s" do |ubuntu_k8s|
  
    config.vm.provider "virtualbox" do |vb|
        vb.name = "k8s-node-3"
        vb.memory = "4096"
        vb.cpus = "4"
     
    end

  end

end

# get node token from master node
sudo cat /var/lib/rancher/k3s/server/node-token

# install k3s on the worker node
curl -sfL https://get.k3s.io | K3S_URL=https://$SERVER_IP:6443 K3S_TOKEN="$NODE_TOKEN" sh -

# enable k3s agent
sudo systemctl enable --now k3s-agent
</code></pre>

### add automatic node upgrade components

<pre><code>
kubectl apply -f https://github.com/rancher/system-upgrade-controller/releases/latest/download/system-upgrade-controller.yaml
</code></pre>

* we can add plans for master and worker nodes (see https://rancher.com/docs/k3s/latest/en/upgrades/automated/#configure-plans)

### upgrade k3s on nodes

<pre><code>
curl -sfL https://get.k3s.io | sh -
</code></pre>


## Troubleshooting

* if after upgrades the nodes cannot connect to the cluster
   * one possible way to fix it is, remove the nodes from the cluster (kubectl delete node xxx), connect to the nodes and manually uninstall k3s and k3s-agent
   * after that, reconnect nodes to cluster with *k3sup join*
* if kubectl outputs "unauthorized" errors, copy the credentials from the server /etc/rancher/k3s/k3s.yaml to the kubeconfig on the managing machine
