# arkade is a tool to install apps to kubernetes

# install arkade

curl -sLS https://dl.get-arkade.dev | sudo sh
sudo chmod +x /usr/local/bin/arkade

# install app (e.g. openfaas)

arkade install openfaas
