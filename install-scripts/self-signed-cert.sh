# Create self-signed certificate

# create root certificate and key
openssl req -x509 \
            -sha256 -days 356 \
            -nodes \
            -newkey rsa:2048 \
            -subj "/CN=sub.example.com/C=EU/L=Konstanz" \
            -keyout rootCA.key -out rootCA.crt

# create private key
openssl genrsa -out server.key 2048

# create configuration for signing request
cat > csr.conf <<EOF
[ req ]
default_bits = 2048
prompt = no
default_md = sha256
req_extensions = req_ext
distinguished_name = dn

[ dn ]
C = EU
ST = Baden-Wuerttemberg
L = Konstanz
O = DevEnv
OU = DevEnv571
CN = sub.devenv.com

[ req_ext ]
subjectAltName = @alt_names

[ alt_names ]
DNS.1 = sub.devenv.com
DNS.2 = www.sub.devenv.com
IP.1 = 192.168.1.5

EOF


# generate signing request using the private key
openssl req -new -key server.key -out server.csr -config csr.conf

# create cert config
cat > cert.conf <<EOF

authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names

[alt_names]
DNS.1 = sub.example.com

EOF

# generate certificate
openssl x509 -req \
    -in server.csr \
    -CA rootCA.crt -CAkey rootCA.key \
    -CAcreateserial -out server.crt \
    -days 365 \
    -sha256 -extfile cert.conf
