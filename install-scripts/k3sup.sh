# install k3sup

curl -SLsf https://get.k3sup.dev | sudo sh
sudo chmod +x /usr/local/bin/k3sup

# generate new ssh key

ssh-keygen -t rsa -b 4096 -C "blubb@example.com"

# move ssh key onto the raspberry pi

ssh-copy-id pi@$PI_IP

# install cluster

k3sup install --ip $SERVER_IP --user pi

# add worker node to cluster

k3sup join --ip $AGENT_IP --server-ip $SERVER_IP --user p
