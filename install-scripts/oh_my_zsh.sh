# install zsh
sudo apt install zsh

# install oh my zsh

sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

# change theme to 'agnoster'

nano ~/.zshrc
