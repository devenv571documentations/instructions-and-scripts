# define versions

export K8S_VERSION="1.25.2-00"
export CONTAINERD_VERSION="1.6.8"
export RUNC_VERSION="1.1.4"
export CNI_PLUGINS_VERSION="1.1.1"

# install prerequisites

sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl

# add google public key and repo

sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list

# install kubeadm, kubelet and kubectl

sudo apt-get update
sudo apt-get install -y kubelet=$K8S_VERSION kubeadm=$K8S_VERSION kubectl=$K8S_VERSION
sudo apt-mark hold kubelet kubeadm kubectl

# Forwarding IPv4 and letting iptables see bridged traffic
sudo modprobe br_netfilter

cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
overlay
br_netfilter
EOF

sudo modprobe overlay
sudo modprobe br_netfilter

# sysctl params required by setup, params persist across reboots
cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-iptables  = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward                 = 1
EOF

# Apply sysctl params without reboot
sudo sysctl --system

# install containerd

wget https://github.com/containerd/containerd/releases/download/v$CONTAINERD_VERSION/containerd-$CONTAINERD_VERSION-linux-amd64.tar.gz
sudo tar Cxzvf /usr/local containerd-$CONTAINERD_VERSION-linux-amd64.tar.gz
sudo rm containerd-$CONTAINERD_VERSION-linux-amd64.tar.gz

# create default containerd config

sudo mkdir /etc/containerd
sudo containerd config default | sudo tee /etc/containerd/config.toml

# set SystemdCGroup to true in containerd config

sudo sed -i 's/SystemdCgroup = false/SystemdCgroup = true/g' /etc/containerd/config.toml

# create containerd service and start it

wget https://raw.githubusercontent.com/containerd/containerd/main/containerd.service
sudo mkdir --parents /usr/local/lib/systemd/system/; sudo mv containerd.service $_

sudo systemctl daemon-reload
sudo systemctl enable --now containerd

# install runc

wget https://github.com/opencontainers/runc/releases/download/v$RUNC_VERSION/runc.amd64
sudo install -m 755 runc.amd64 /usr/local/sbin/runc
sudo rm runc.amd64

# install CNI plugins

sudo mkdir -p /opt/cni/bin
wget https://github.com/containernetworking/plugins/releases/download/v$CNI_PLUGINS_VERSION/cni-plugins-linux-amd64-v$CNI_PLUGINS_VERSION.tgz
sudo tar Cxzvf /opt/cni/bin cni-plugins-linux-amd64-v$CNI_PLUGINS_VERSION.tgz
sudo rm cni-plugins-linux-amd64-v$CNI_PLUGINS_VERSION.tgz
