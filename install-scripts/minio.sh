####################
# install using helm
####################

# add stable repo to helm

helm repo add stable https://kubernetes-charts.storage.googleapis.com

# install minio

helm install minio stable/minio

## with arm

helm install minio --set image.repository=jessestuart/minio --set image.tag=latest-arm stable/minio

## read secrets

kubectl get secret minio --template={{.data.accesskey}} | base64 --decode
kubectl get secret minio --template={{.data.secretkey}} | base64 --decode
