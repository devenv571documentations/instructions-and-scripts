# install openFAAS cli
curl -sL https://cli.openfaas.com | sudo sh

# install arkade
curl -SLsf https://dl.get-arkade.dev/ | sudo sh

# install openfaas
arkade install openfaas

# access openfaas gateway
# kubectl port-forward -n openfaas svc/gateway 8080:8080 &

# get password for gateway user (admin)
# kubectl get secret basic-auth -n openfaas -o jsonpath="{.data.basic-auth-password}" | base64 --decode

# create function
# faas-cli new --lang python-armhf hello-world

# build function (make sure to have the correct image name set)
# faas-cli build -f hello-world.yml

# push to registry
# faas-cli push -f hello-world.yml

# push to cluster
# faas-cli deploy -f hello-world.yml
