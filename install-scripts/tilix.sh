# add repo to /etc/apt/sources.list
deb http://de.archive.ubuntu.com/ubuntu bionic main universe

# install linix
sudo apt install tilix

# if config errors occur when opening preferences
# edit ~/.bashrc or ~/.zshrc and add the following
if [ $TILIX_ID ] || [ $VTE_VERSION ]; then
        source /etc/profile.d/vte.sh
fi

# also execute the following line to create a symlink
sudo ln -s /etc/profile.d/vte-2.91.sh /etc/profile.d/vte.sh

