# install k9s
export K9S_VERSION=0.27.2
wget https://github.com/derailed/k9s/releases/download/v$K9S_VERSION/k9s_Linux_amd64.tar.gz
mkdir /tmp/k9s
tar -xzvf k9s_Linux_amd64.tar.gz -C /tmp/k9s/
sudo mv /tmp/k9s/k9s /usr/local/bin/k9s
sudo rm k9s_Linux_amd64.tar.gz
sudo rm -r /tmp/k9s/

