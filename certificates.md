# Create self-signed certificate

* create root (CA) certificate and key (it is self-signed by nature)
<pre><code>
openssl req -x509 \
            -sha256 -days 356 \
            -nodes \
            -newkey rsa:2048 \
            -subj "/CN=devenv.com/C=EU/L=Konstanz" \
            -keyout rootCA.key -out rootCA.crt
<code></pre>

* **note**: another way would be to create the private key and then a csr (signing request) to create the certificate in 2 steps

* create private key (for the server you want to create a certificate for)

<pre><code>
openssl genrsa -out server.key 2048
<code></pre>

* create configuration for signing request

<pre><code>
cat > csr.conf <<EOF
[ req ]
default_bits = 2048
prompt = no
default_md = sha256
req_extensions = req_ext
distinguished_name = dn

[ dn ]
C = EU
ST = Baden-Wuerttemberg
L = Konstanz
O = DevEnv
OU = DevEnv571
# this is the main name the entity is known as
CN = sub.devenv.com

[ req_ext ]
subjectAltName = @alt_names

# these are all the additional names the entity is known as
[ alt_names ]
DNS.1 = sub.devenv.com
DNS.2 = www.sub.devenv.com
IP.1 = 192.168.1.5

EOF
<code></pre>


* generate signing request using the private key
<pre><code>
openssl req -new -key server.key -out server.csr -config csr.conf
<code></pre>

* create cert config

<pre><code>
cat > cert.conf <<EOF

authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names

[alt_names]
DNS.1 = sub.devenv.com

EOF
<code></pre>

* generate certificate by signing the request with the CA cert (and key)

<pre><code>
openssl x509 -req \
    -in server.csr \
    -CA rootCA.crt -CAkey rootCA.key \
    -CAcreateserial -out server.crt \
    -days 365 \
    -sha256 -extfile cert.conf
<code></pre>


* add k8s secret of certificate

<pre><code>
kubectl create secret tls sample-app-tls \
    --namespace dev \
    --key server.key \
    --cert server.crt
<code></pre>

* read certificate

```
openssl x509 -in my-cert.pem -text
```

* add CA certificate to trusted certificates

```
sudo cp /vagrant/rootCA.crt /usr/local/share/ca-certificates/ && sudo update-ca-certificates --fresh
```
