# OpenFaas on Raspberry Pi

## Building ARM Docker Images on x86 Architecture

### Install QEMU

<pre><code>
apt-get install qemu-user qemu-user-static
</code></pre>

* the binary **/usr/bin/qemu-user-static** has to be copied into the Docker image

<pre><code>
COPY /path/to/qemu-arm-static /usr/bin/qemu-arm-static
</code></pre>

## Python templates

* the image for the class-watchdog of openFaas has to be changed to be ARM compatible (digest for linux/arm)
* the Python image has to be changed as well (digest for linux/arm/v7)
* e.g.

<pre><code>
FROM openfaas/classic-watchdog@sha256:604bc53cd073c6107c0a512af279b2399e164366e060745140bcda8775af76fc as watchdog

FROM python@sha256:d6478625c11b17c8744efa9602a5626f51933dc4718329039855ff522083884c
</code></pre>

* pip does not include a lot of packages for arm
* therefore, use https://www.piwheels.org/
* create a config for pip with the following content

<pre><code>
[global]
extra-index-url=https://www.piwheels.org/simple
</code></pre>

* and add the config to the following location

<pre><code>
COPY pip.conf /etc/pip.conf
</code></pre>

### OpenCV

* to use OpenCV on Raspberry, we need to use piwheels
* we also need to install a couple of dependencies
** https://blog.piwheels.org/new-opencv-builds/
* newest version of OpenCV not working on Raspberry
** we need version opencv-python==3.4.6.27

* https://ubuntu.pkgs.org
