# keycloak

docker run -d -p 8080:8080 -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin jboss/keycloak

# rabbitmq

docker run -d --hostname rabbitmq --name rabbitmq -p 15672:15672 -p 5672:5672 rabbitmq:3-management

# postgres

docker run --name postgres -e POSTGRES_PASSWORD=postgres -p 5432:5432 -d postgres

# pgadmin4

docker run --name pgadmin -p 5000:80 -e PGADMIN_DEFAULT_EMAIL=blubb@gmail.com -e PGADMIN_DEFAULT_PASSWORD=postgres -d dpage/pgadmin4
