# delete service

kubectl delete -f postgres-service.yaml

# delete stateful set

kubectl delete -f postgres-statefulset.yaml

# delete volume and pvc

kubectl delete -f postgres-pv-pvc.yaml

# delete config map

kubectl delete -f postgres-configmap.yaml
