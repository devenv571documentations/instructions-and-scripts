# create config map

kubectl create -f postgres-configmap.yaml

# create volume and pvc

kubectl create -f postgres-pv-pvc.yaml

# create stateful set

kubectl create -f postgres-statefulset.yaml

# create service

kubectl create -f postgres-service.yaml
