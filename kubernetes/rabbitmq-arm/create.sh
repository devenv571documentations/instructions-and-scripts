# create volume and pvc

kubectl create -f rabbitmq-pv-pvc.yaml

# create stateful set

kubectl create -f rabbitmq-statefulset.yaml

# create service

kubectl create -f rabbitmq-service.yaml
