# delete service

kubectl delete -f rabbitmq-service.yaml

# delete stateful set

kubectl delete -f rabbitmq-statefulset.yaml

# delete volume and pvc

kubectl delete -f rabbitmq-pv-pvc.yaml
